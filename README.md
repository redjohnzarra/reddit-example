Reddit Test Web App Using React, Redux, Snoowrap
======================================================

How to use:
-----------

Clone this project
------------------
```
git clone https://gitlab.com/redjohnzarra/reddit-example.git
```
Go to the cloned project
------------------------
```
cd reddit-example
```
Install node modules
--------------------
```
npm install or yarn
```
Generate Anonymous Auth Token [HERE](https://not-an-aardvark.github.io/reddit-oauth-helper/)
--------------------
Copy the generated token and paste it inside **src/constants/index.js**

Run the project
---------------
```
npm start or yarn start
```

export const BEST = {
  display: 'Best',
  value: 'confidence',
};
export const TOP = {
  display: 'Top',
  value: 'top',
};
export const NEW = {
  display: 'New',
  value: 'new',
};
export const CONTROVERSIAL = {
  display: 'Controversial',
  value: 'controversial',
};
export const OLD = {
  display: 'Old',
  value: 'old',
};
export const QA = {
  display: 'Q&A',
  value: 'qa',
};

import Snoowrap from 'snoowrap';
import { AUTH_TOKEN } from '../constants/index';

const getSnoowrap = () => {
  return new Snoowrap({
    accessToken: AUTH_TOKEN,
  });
};

export default getSnoowrap;

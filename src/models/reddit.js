import update from 'immutability-helper';
import getSnoowrap from '../utils/snoowrapper';
import * as sortTypes from '../constants/sort_types';

export default {
  namespace: 'reddit',
  state: {
    loading: false,
    posts: [],
    postInfo: {},
    tokenExpired: false,
  },

  reducers: {
    getPostsSuccess(state, { payload }) {
      return update(state, {
        posts: {
          $set: payload,
        },
      });
    },
    getPostInfoSuccess(state, { payload }) {
      return update(state, {
        postInfo: {
          $set: payload,
        },
      });
    },
    updateLoadingState(state, { payload }) {
      return update(state, {
        loading: {
          $set: payload,
        },
      });
    },
    setTokenExpired(state, { payload }) {
      return update(state, {
        tokenExpired: {
          $set: payload,
        },
      });
    },
  },

  effects: {
    // eslint-disable-next-line func-names
    getPosts: [function* ({ sortType }, { put }) {
      try {
        yield put({ type: 'updateLoadingState', payload: true });
        yield put({ type: 'setTokenExpired', payload: false });
        let postList = [];
        if (sortType === sortTypes.HOT) {
          yield getSnoowrap().getHot().then((posts) => {
            postList = posts;
          });
        } else if (sortType === sortTypes.NEW) {
          yield getSnoowrap().getNew().then((posts) => {
            postList = posts;
          });
        } else if (sortType === sortTypes.CONTROVERSIAL) {
          yield getSnoowrap().getControversial().then((posts) => {
            postList = posts;
          });
        } else if (sortType === sortTypes.TOP) {
          yield getSnoowrap().getTop().then((posts) => {
            postList = posts;
          });
        } else if (sortType === sortTypes.RISING) {
          yield getSnoowrap().getRising().then((posts) => {
            postList = posts;
          });
        }
        // yield get('https://jsonplaceholder.typicode.com/todos/1').then((response) => {
        //   console.log('response here', response);
        // });
        yield put({ type: 'updateLoadingState', payload: false });
        yield put({ type: 'getPostsSuccess', payload: postList });
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log('error here dapit', error);
        yield put({ type: 'updateLoadingState', payload: false });
        yield put({ type: 'setTokenExpired', payload: true });
      }
    }, { type: 'takeLatest' }],
    // eslint-disable-next-line func-names
    getPostInfo: [function* ({ postId, commentSort }, { put }) {
      try {
        yield put({ type: 'updateLoadingState', payload: true });
        yield put({ type: 'setTokenExpired', payload: false });
        let postInfo = {};
        yield getSnoowrap().getSubmission(postId).setSuggestedSort(commentSort).fetch()
          .then((post) => {
            postInfo = post;
          });
        yield put({ type: 'updateLoadingState', payload: false });
        yield put({ type: 'getPostInfoSuccess', payload: postInfo });
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log('error', error);
        yield put({ type: 'updateLoadingState', payload: false });
        yield put({ type: 'setTokenExpired', payload: true });
      }
    }, { type: 'takeLatest' }],
    // eslint-disable-next-line func-names
    clearPostInfo: [function* (_, { put }) {
      yield put({ type: 'getPostInfoSuccess', payload: {} });
    }],
  },

  subscriptions: {},
};

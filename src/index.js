import dva from 'dva';
import './index.css';
import { createBrowserHistory } from 'history';
import 'antd/dist/antd.less';
import { createLogger } from 'redux-logger';
import redditModel from './models/reddit';
import router from './router';

import './layout.less';

// 1. Initialize
let app = null;
if (process.env.NODE_ENV !== 'production') {
  app = dva({
    history: createBrowserHistory(),
    onAction: createLogger({
      level: 'info',
      collapsed: true,
    }),
  });
} else {
  app = dva({
    history: createBrowserHistory(),
  });
}


// 2. Plugins
// app.use({});


// 3. Model
app.model(redditModel);

// 4. Router
app.router(router);

// 5. Start
app.start('#root');

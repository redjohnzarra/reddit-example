import React from 'react';
import {
  Spin,
} from 'antd';

const Loading = () => (
  <div
    className="center-child-cont"
    style={{
      // eslint-disable-next-line no-undef
      height: window.innerHeight - 300,
    }}
  >
    <Spin size="large" />
  </div>
);

export default Loading;

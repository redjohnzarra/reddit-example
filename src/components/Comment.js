/* eslint-disable react/no-danger */
import React from 'react';
import {
  Collapse, Tooltip,
} from 'antd';
import moment from 'moment';
import _ from 'lodash';

const { Panel } = Collapse;

const CreateComment = (comment) => {
  return (
    <div className="c-black">
      <div>
        <b className="primary-link">{_.get(comment, 'author.name')}</b>
        <span> • </span>
        <Tooltip title={moment.unix(_.get(comment, 'created_utc')).format('YYYY-MM-DD HH:mm:ss')}>
          {moment.unix(_.get(comment, 'created_utc')).fromNow()}
        </Tooltip>
      </div>
      <div className="b-word">
        {<div dangerouslySetInnerHTML={{ __html: _.get(comment, 'body_html') }} />}
        {/* {_.get(comment, 'body')} */}
      </div>
    </div>
  );
};

const Comment = ({ comment }) => {
  const hasChild = !_.isEmpty(comment.replies);
  if (hasChild) {
    return (
      <Collapse key={_.get(comment, 'id')} bordered={false} defaultActiveKey={[_.get(comment, 'id')]}>
        <Panel
          key={_.get(comment, 'id')}
          header={CreateComment(comment)}
        >
          {
            _.map(comment.replies, (reply, idx) => {
              return (
                <Comment
                  key={idx}
                  comment={reply}
                />
              );
            })
          }
        </Panel>
      </Collapse>
    );
  } else {
    return (
      <Panel
        key={_.get(comment, 'id')}
        className="no-child-comment"
        header={CreateComment(comment)}
      />
    );
  }
};

export default Comment;

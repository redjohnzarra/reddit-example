/* eslint-disable react/no-danger */
import React from 'react';
import _ from 'lodash';
import {
  Card,
} from 'antd';

import {
  buildSubRedditHeader, buildImages, buildRenderHTML, buildAuthor, buildSharePopover,
} from './CommonCardComponents';

const Post = ({ post, goToPost }) => {
  const previewImages = _.get(post, 'preview.images', null);
  const selfText = _.get(post, 'selftext_html', null);
  const authorName = _.get(post, 'author.name');
  const mediaEmbed = _.get(post, 'media_embed', {});
  return (
    <Card
      hoverable
      onClick={goToPost(post.id)}
    >
      {buildSubRedditHeader(post)}
      <h2>{post.title}</h2>
      {
        mediaEmbed
          ? buildRenderHTML(_.get(mediaEmbed, 'content'), 'center')
          : null
      }
      {
        previewImages && _.isEmpty(mediaEmbed) ? buildImages(previewImages) : null
      }
      {
        selfText ? buildRenderHTML(selfText, 'left') : null
      }
      {
        authorName ? buildAuthor(authorName) : null
      }
      <br />
      <div>
        {buildSharePopover(post.title)}
      </div>
    </Card>
  );
};

export default Post;

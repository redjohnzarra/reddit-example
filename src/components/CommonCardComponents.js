/* eslint-disable no-undef */
/* eslint-disable react/no-danger */
import React from 'react';
import _ from 'lodash';
import {
  List, Icon, Col, Modal, Input, Button, Popover,
} from 'antd';

const { TextArea } = Input;

export const buildRenderHTML = (html, textAlign) => {
  const style = { textAlign };
  return (
    <div style={style}>
      {<div dangerouslySetInnerHTML={{ __html: html }} />}
    </div>
  );
};

export const buildAuthor = (authorName) => {
  return (
    <div className="float-right">
      <span>
        Posted by:&nbsp;
        <b>{authorName}</b>
      </span>
    </div>
  );
};

export const buildImages = (images) => {
  const imagesComponent = [];
  _.map(images, (image) => {
    // eslint-disable-next-line jsx-a11y/alt-text
    imagesComponent.push(
      <img
        alt=""
        key={image.id}
        className="image-previews"
        src={_.get(image, 'source.url')}
      />,
    );
  });

  return (
    <div className="previews-container">
      {imagesComponent}
    </div>
  );
};

export const buildSubRedditHeader = (post) => {
  return (
    <b className="primary-link">{_.get(post, 'subreddit_name_prefixed')}</b>
  );
};

export const copyToClipboard = (url) => {
  return (e) => {
    e.preventDefault();
    e.stopPropagation();
    const el = document.createElement('textarea');
    el.value = url;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    Modal.success({
      title: 'Success',
      content: 'Link copied to clipboard!',
    });
  };
};

export const showEmbedCode = (url, title) => {
  return (e) => {
    e.preventDefault();
    e.stopPropagation();
    Modal.info({
      title: 'Embed Code',
      content: (
        <div>
          <div>Copy this embed code to your site!</div>
          <div>
            <TextArea
              autosize
              value={`<blockquote><a href="${url}">${title}</a>`}
            />
          </div>
        </div>
      ),
    });
  };
};

export const buildSharePopover = (title) => {
  const url = window.location.href;
  const data = [
    {
      title: 'Copy Link',
      icon: 'link',
      onClick: copyToClipboard(url),
    },
    {
      title: 'Embed',
      icon: 'code',
      onClick: showEmbedCode(url, title),
    },
  ];

  return (
    <Popover
      content={(
        <List
          bordered
          size="small"
          dataSource={data}
          renderItem={item => (
            <List.Item className="list-hover" onClick={item.onClick}>
              <Col>
                <Icon type={item.icon} />
                &nbsp;
              </Col>
              <Col>
                {item.title}
              </Col>
            </List.Item>
          )}
        />
      )}
      trigger="click"
    >
      <Button
        className="footer-link"
        icon="share-alt"
        type="link"
        onClick={(e) => {
          e.preventDefault();
          e.stopPropagation();
        }}
      >
        Share
      </Button>
    </Popover>
  );
};

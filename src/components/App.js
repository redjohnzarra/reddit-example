/* eslint-disable no-undef */
import React from 'react';
import {
  Layout, LocaleProvider,
} from 'antd';

import { connect } from 'dva';

import enUS from 'antd/lib/locale-provider/en_US';
import RouteWithSubRoutes from '../routes/RouteWithSubRoutes';

const { Header, Content } = Layout;

const goToHome = (history) => {
  return () => history.push('/');
};

function App({ routes, history }) {
  return (
    <LocaleProvider locale={enUS}>
      <Layout className="main-layout">
        <Header className="bg-white" onClick={goToHome(history)}>
          <h2 className="primary-color">Reddit Sample</h2>
        </Header>
        <Content className="content-layout" style={{ minHeight: window.innerHeight - 64 }}>
          {
            routes.map((route, i) => {
              // eslint-disable-next-line react/no-array-index-key
              return <RouteWithSubRoutes key={i} {...route} />;
            })
          }
        </Content>
      </Layout>
    </LocaleProvider>
  );
}

function mapStateToProps(state) {
  return { auth: state.auth };
}


export default connect(mapStateToProps)(App);

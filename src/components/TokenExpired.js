/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import {
  Alert,
} from 'antd';

const Loading = () => (
  <div
    className="center-child-cont"
    style={{
      // eslint-disable-next-line no-undef
      height: window.innerHeight - 300,
    }}
  >
    <Alert
      message="Token has Expired"
      description={(
        <div>
          Please generate a new anonymous token
          {' '}
          <a
            href="https://not-an-aardvark.github.io/reddit-oauth-helper/"
            target="_blank"
            rel="noopener noreferrer"
          >
            HERE
          </a>
          {' '}
          and paste it in the
          {' '}
          <b>constants/index.js</b>
          {' '}
          file.
        </div>
      )}
      type="info"
    />
  </div>
);

export default Loading;

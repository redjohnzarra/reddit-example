import React from 'react';
import { connect } from 'dva';
import {
  Row, Col,
} from 'antd';
import _ from 'lodash';
import Comment from './Comment';

class Comments extends React.Component {
  buildComments = (comments) => {
    return _.map(comments, (comment) => {
      return <Comment key={comment.id} comment={comment} />;
    });
  }

  render() {
    const { comments } = this.props;
    return (
      <Row>
        <Col className="bg-white">
          {this.buildComments(comments)}
          <br />
        </Col>
      </Row>
    );
  }
}

export default connect(null, null)(Comments);

/* eslint-disable react/no-danger */
import React from 'react';
import { connect } from 'dva';
import {
  Row, Col, Card, Divider, Icon, Button, Tag, Select, Spin,
} from 'antd';
import _ from 'lodash';
import moment from 'moment';
import Comments from '../components/Comments';
import Loading from '../components/Loading';
import TokenExpired from '../components/TokenExpired';

import * as sortCommentTypes from '../constants/sort_comment_types';

import {
  buildImages, buildRenderHTML, buildSharePopover,
} from '../components/CommonCardComponents';

const { Option } = Select;

class SinglePost extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      commentSort: sortCommentTypes.BEST.value,
    };
  }

  componentWillMount() {
    this.getPostInformation();
  }

  getPostInformation = () => {
    const { commentSort } = this.state;
    const postId = _.get(this.props, 'match.params.id', null);
    if (postId) {
      // eslint-disable-next-line react/destructuring-assignment
      this.props.dispatch({
        type: 'reddit/getPostInfo',
        postId,
        commentSort,
      });
    }
  }

  goBack = () => {
    // eslint-disable-next-line react/destructuring-assignment
    this.props.history.goBack();
  }

  buildCard = (postInfo) => {
    const linkFlairText = _.get(postInfo, 'link_flair_text', null);
    const previewImages = _.get(postInfo, 'preview.images', null);
    const selfText = _.get(postInfo, 'selftext_html', null);
    const mediaEmbed = _.get(postInfo, 'media_embed', {});
    return (
      <Card title={this.buildCardHeader(postInfo)}>
        <h2>{_.get(postInfo, 'title', '')}</h2>
        {
          linkFlairText ? this.buildLinkFlairText(linkFlairText) : null
        }
        <br />
        <br />
        {
          mediaEmbed
            ? buildRenderHTML(_.get(mediaEmbed, 'content'), 'center')
            : null
        }
        {
          previewImages && _.isEmpty(mediaEmbed) ? buildImages(previewImages) : null
        }
        {
          selfText ? buildRenderHTML(selfText) : null
        }
        <br />
        <div>
          {buildSharePopover(postInfo.title)}
        </div>
      </Card>
    );
  }

  buildCardHeader = (postInfo) => {
    return (
      <Col span={24}>
        <div>
          <Col span={12}>
            <b className="primary-link">{_.get(postInfo, 'subreddit_name_prefixed', '')}</b>
          </Col>
          <Col span={12} className="col-card-header">
            {'Posted By '}
            {_.get(postInfo, 'author.name', '')}
            {' on '}
            {moment.unix(_.get(postInfo, 'created_utc', null)).format('MMMM DD, YYYY HH:mm')}
          </Col>
        </div>
      </Col>
    );
  }

  buildLinkFlairText = (linkFlairText) => {
    return (
      <Col span={24}>
        <Tag>{linkFlairText}</Tag>
      </Col>
    );
  }

  onChangeCommentSort = (value) => {
    this.setState({
      commentSort: value,
    }, () => {
      this.getPostInformation();
    });
  }

  buildSortCommentOptions = () => {
    return _.map(sortCommentTypes, (type) => {
      return (
        <Option key={type.value} value={type.value}>{type.display}</Option>
      );
    });
  }

  render() {
    const { isLoading, postInfo, isTokenExpired } = this.props;
    const { commentSort } = this.state;
    if (isLoading && _.isEmpty(postInfo)) {
      return (
        <Loading />
      );
    }

    if (isTokenExpired) {
      return <TokenExpired />;
    }

    return (
      <Col span={20} offset={2}>
        <br />
        <Row>
          <Col>
            <Button type="link" onClick={this.goBack}>
              <Icon type="arrow-left" />
              {'   '}
              Go Back
            </Button>
          </Col>
        </Row>
        <br />
        <Row>
          {this.buildCard(postInfo)}
          <Divider />
          <Row className="comments-container">
            <Col span={12}>
              <h3>
                Comments
              </h3>
            </Col>
            <Col span={12} className="ta-right">
              <b>Sort Comments By:</b>
              &emsp;
              <Select
                dropdownMatchSelectWidth={false}
                value={commentSort}
                onChange={this.onChangeCommentSort}
              >
                {this.buildSortCommentOptions()}
              </Select>
            </Col>
          </Row>
          <Spin size="large" spinning={isLoading}>
            <Row>
              <Col span={24}>
                <Comments
                  comments={_.get(postInfo, 'comments', [])}
                />
              </Col>
            </Row>
          </Spin>
        </Row>
      </Col>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.reddit.loading,
    postInfo: state.reddit.postInfo,
    isTokenExpired: state.reddit.tokenExpired,
  };
}

export default connect(mapStateToProps)(SinglePost);

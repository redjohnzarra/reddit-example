import React from 'react';
import { connect } from 'dva';
import {
  Row, Col, Select, Spin,
} from 'antd';
import _ from 'lodash';
import Post from '../components/Post';
import Loading from '../components/Loading';
import TokenExpired from '../components/TokenExpired';
import * as sortTypes from '../constants/sort_types';

const { Option } = Select;

class Main extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submissionSort: sortTypes.HOT,
    };
  }

  componentWillMount() {
    this.getPosts();
  }

  getPosts = () => {
    const { submissionSort } = this.state;
    // eslint-disable-next-line react/destructuring-assignment
    this.props.dispatch({ type: 'reddit/getPosts', sortType: submissionSort });
  }

  generatePostsList = () => {
    const { posts } = this.props;
    return posts.map((post) => {
      return (
        <Col key={post.id} span={24} className="post-col-cont">
          <Post
            post={post}
            goToPost={this.goToPost}
          />
        </Col>
      );
    });
  }

  goToPost = (id) => {
    return () => {
      // eslint-disable-next-line react/destructuring-assignment
      this.props.dispatch({ type: 'reddit/clearPostInfo' });
      // eslint-disable-next-line react/destructuring-assignment
      this.props.history.push(`/post/${id}`);
    };
  }

  onChangeSort = (value) => {
    this.setState({
      submissionSort: value,
    }, () => {
      this.getPosts();
    });
  }

  buildSortOptions = () => {
    return _.map(sortTypes, (type) => {
      return (
        <Option key={type} value={type}>{type}</Option>
      );
    });
  }

  render() {
    const { isLoading, isTokenExpired, posts } = this.props;
    const { submissionSort } = this.state;
    if (isLoading && _.isEmpty(posts)) {
      return <Loading />;
    }
    if (isTokenExpired) {
      return <TokenExpired />;
    }
    return (
      <Col span={16} offset={4}>
        <br />
        <Row>
          <Col className="ta-right">
            <b>Sort By:</b>
            &emsp;
            <Select
              dropdownMatchSelectWidth={false}
              value={submissionSort}
              onChange={this.onChangeSort}
            >
              {this.buildSortOptions()}
            </Select>
          </Col>
        </Row>
        <Spin size="large" spinning={isLoading}>
          <Row>
            {this.generatePostsList()}
          </Row>
        </Spin>
      </Col>
    );
  }
}

function mapStateToProps(state) {
  return {
    posts: state.reddit.posts,
    isLoading: state.reddit.loading,
    isTokenExpired: state.reddit.tokenExpired,
  };
}

export default connect(mapStateToProps)(Main);

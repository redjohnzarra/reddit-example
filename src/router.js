import React from 'react';
import { Router, Route, Redirect } from 'dva/router';

import RouteWithSubRoutes from './routes/RouteWithSubRoutes';
import App from './components/App';
import Main from './routes/Main';
import SinglePost from './routes/SinglePost';

function RouterConfig({ history }) {
  const routes = [
    {
      path: '/',
      component: App,
      routes: [
        {
          path: '/main',
          exact: true,
          component: Main,
        }, {
          path: '/post/:id',
          exact: true,
          component: SinglePost,
        },
      ],
    },
  ];

  return (
    <Router history={history}>
      <div>
        <div>
          {routes.map((route, i) => (
            // eslint-disable-next-line react/no-array-index-key
            <RouteWithSubRoutes key={i} {...route} />
          ))}
          <Route
            exact
            path="/"
            render={() => (
              <Redirect to="/main" />
            )}
          />
        </div>
      </div>
    </Router>
  );
}

export default RouterConfig;
